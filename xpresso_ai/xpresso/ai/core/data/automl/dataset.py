""" Class design for Dataset"""
import copy
import pickle
from abc import abstractmethod
import datetime
import pandas as pd
import os

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    SerializationFailedException, DeserializationFailedException
from xpresso.ai.core.data.exploration.structured_dataset_info import StructuredDatasetInfo
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser

__all__ = ['AbstractDataset']
__author__ = 'Srijan Sharma'

logger = XprLogger()


class AbstractDataset(object):
    """ Dataset is an abstract storage class. It is responsible for complete
    lifecycle of the a automl. It start with importing from the source,
    saving/loading from the local, performing exploration and analysis
    over the data
    """

    def __init__(self, dataset_name: str = "default",
                 description: str = "This is a automl",
                 project_name: str = "default_project",
                 created_by: str = "default",
                 config_path: str = XprConfigParser.DEFAULT_CONFIG_PATH):
        self.config = XprConfigParser(config_file_path=config_path)

        self.data = pd.DataFrame()
        self.name = dataset_name
        self.type = DatasetType.STRUCTURED
        self.description = description
        self.num_records = len(self.data)
        self.created_on = datetime.datetime.now().date()
        self.created_by = created_by
        self.project = project_name
        self.repo = "default"
        self.branch = "master"
        self.version = 1
        self.tag = "1.0.0"
        self.info = StructuredDatasetInfo()
        self.local_storage_required = False
        self.sample_percentage = 100.00
        self.CONTROLLER_SECTION = "controller"
        self.CLIENT_PATH = "client_path"
        self.path = os.path.join(
            os.path.expanduser('~'),
            self.config[self.CONTROLLER_SECTION][self.CLIENT_PATH])
        self.token_file = '{}.current'.format(self.path)

    @abstractmethod
    def import_dataset(self, data_source, local_storage_required: bool = False,
                       sample_percentage: float = 100):
        """
        Fetches automl from multiple data sources and loads them
        into a automl

        Args:
            data_source(str): string path or uri of the data source
            local_storage_required(bool):
            sample_percentage(sbool):
        """

    @abstractmethod
    def save(self):
        """ Serialize the automl and store it into a local storage"""

    @abstractmethod
    def load(self, pickle_file_path: str):
        """
        Load the data set from local storage and deserialize to update
        the automl

        pickle_file_path
            path of the pickle file from which automl is loaded
        """
        if not pickle_file_path:
            DeserializationFailedException("Invalid pickle file provided")

        try:
            with open(pickle_file_path, "rb") as pickle_fs:
                serialized_data = pickle_fs.read()
                dataset_obj = self.deserialize(serialized_data)
                self.import_from_dataset(dataset_obj)
                return True
        except FileNotFoundError as err:
            raise DeserializationFailedException(f"File not found at"
                                                 f" {pickle_file_path}")

    @abstractmethod
    def diff(self, new, output_path=None):
        """ Find the diff between two automl"""

    def serialize(self) -> bytes:
        """ Serialize the automl object into a byte order string """
        try:
            serialized_byte = pickle.dumps(self,
                                           protocol=pickle.HIGHEST_PROTOCOL)
            return serialized_byte
        except (pickle.PickleError, pickle.PicklingError):
            logger.error("Failed to serialize the automl object")
            raise SerializationFailedException("Serialization failed")

    def deserialize(self, serialized_byte: bytes, update_self=True) -> object:
        """
        Deserialize the automl object from byte order string into the
        automl object

        Args:
            serialized_byte: byte ordered string of a serialized automl object
            update_self: If set to True, update the properties of self object

        Returns:
            automl object
        """
        try:
            deserialize_obj = pickle.loads(serialized_byte)
            if update_self:
                self.import_from_dataset(deserialize_obj)
            return deserialize_obj
        except (pickle.PickleError, pickle.PicklingError):
            logger.error("Failed to  deserialize the byte string")
            raise DeserializationFailedException("Deserialization failed")

    def get_local_storage_path(self):
        """ Returns the path of the local storage"""
        # xpresso platform standard path where token file is saved
        local_storage = os.path.join(self.project, "datasets", self.name)
        os.makedirs(local_storage, exist_ok=True)
        return local_storage

    def get_pickle_file_path(self, number=1):
        """
        Get the pickle file path
        Args:
            number: Specify which version the pickle file. default=1

        Returns:
            str: absolute pickle file path
        """
        return self.get_pickle_pattern() % '{0:0>5}'.format(number)

    def get_pickle_pattern(self):
        """ Generates a name pattern for all the pickle file. This is
         used to generate the absolute file path maintaining the versions"""
        parent_dir = self.get_local_storage_path()
        return os.path.join(parent_dir, f"{self.name}_dataset__%s.pkl")

    def import_from_dataset(self, dataset):
        """
        Import properties of automl from another automl

        Args:
            dataset: source automl. Properties of these automl will be
                     updated in the current automl
        """
        self.__dict__ = copy.deepcopy(dataset.__dict__)
